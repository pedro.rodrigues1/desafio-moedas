from sys import argv


def retorna_moeda(vlr):

    moedas = [1, 0.5, 0.25, 0.10, 0.05, 0.01]
    retorno = ["Um real","cinquenta centavos","vinte e cinco centavos","dez centavos","cinco centavos","um centavo"]
    resultado = []
    i = 0
    try:
        vlr = float(vlr)
        # pego o maior vlr em moedas
        while vlr != 0: 
            vlr = round(vlr,2)
            if vlr >= moedas[i]: 
                resultado.append(retorno[i])
                vlr -= moedas[i]
            else:
                i += 1
        # pego quantas vezes as moedas se repetem e retorno em um dicionário
        count_result = {}
        for result in resultado:
            count_result[result] = 1 if count_result.get(result) is None else count_result[result] + 1
        return count_result

    except:ValueError
    print("Digite o vlr com ponto! Não use vírgula! Não use string!")


if __name__ == "__main__":
    try:    
        vlr = argv[1] 
        print(f'moeda / quantidade = {retorna_moeda(vlr)}')
    except:
        print("Digite um vlr!")
    