import unittest
from retorna_moedas_pedro import retorna_moeda


class TestRetorna_moeda(unittest.TestCase):

    def test1(self):
        print("Teste 2.58 reais ")
        vlr = 2.58
        teste = retorna_moeda(vlr)
        resultado = {'Um real': 2, 'cinquenta centavos': 1, 'cinco centavos': 1, 'um centavo': 3}
        self.assertEqual(teste,resultado)
 
    def test2(self):
        print("Teste 0 reais")
        vlr = 0
        teste = retorna_moeda(vlr)
        resultado = {}
        self.assertEqual(teste,resultado)

    def test3(self):
        print("Teste valor com vírgula: ", end="")
        vlr = 2,55
        teste = retorna_moeda(vlr)
        resultado = None
        self.assertEqual(teste,resultado)

    def test4(self):
        print("Teste valor Nulo: ", end="")
        vlr = None
        teste = retorna_moeda(vlr)
        resultado = None
        self.assertEqual(teste,resultado)

    def test5(self):
        print("Teste 40.68 reais ")
        vlr = 40.68
        teste = retorna_moeda(vlr)
        resultado = {'Um real': 40, 'cinquenta centavos': 1, 'dez centavos': 1, 'cinco centavos': 1, 'um centavo': 3}
        self.assertEqual(teste,resultado)
    
    def test6(self):
        print("Teste valor Negativo: ", end="")
        vlr = -3
        teste = retorna_moeda(vlr)
        resultado = None
        self.assertEqual(teste,resultado)

   
if __name__ == '__main__':
    unittest.main()
